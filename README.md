# Scrum
Scrum is a framework or a process that brings easier and more organized management of teams that work on complex problems. The main idea of scrum is to bring products with the highest possible value and quality as well as very reliable and fast delivery of smaller increments of the product. With scrum, not only the product owner, but the team working on the product will be in a much better position because through the system they can communicate features and functionalities better as well as set and manage expectations. 
## Scrum process flow
Scrum consists of a few mechanisms and rituals that need to be followed in order to have the complete process flow. Those are:

* **Product Backlog** - Board with all Features, Epics and User Stories needed to be implemented. This product backlog is literally what the name suggests - a backlog of all work that needs to be done. 
* **Sprint** - An interval of time ( usually few weeks or a month ) decided and set for completing a determined number of tasks that at the end, complete one incremental piece of work on the product. The goal of the sprint and the incremental piece of work should not be changed in the duration of the sprint, but the scope or requirements on some of the User Stories may change in coordination with the Product Owner and Team.
  * **Sprint Planning** - Sprint planning is a process where the scrum team gathers and decides what is the goal for the following sprint, what User Stories can be added, what is their complexity, if the work is sufficient and if everything in the end will result in a complete incremental unit that can be shipped or attached to the product that is currently in development. User stories and tasks are organized by analyzing the Product Backlog. User stories can be broken down in to smaller, more manageable bits if there are stories that have too much complexity or take too much time to be finished. Teams decide complexity in different ways. Some with time, others with generic Low, Medium, High complexity and some with Points. The goal is to give every User Story a complexity value that everyone agrees, so that expectations are set.
  * **Sprint Backlog** - The Sprint backlog or Sprint Board is the place where all the User Stories decided on the Sprint Planning are added. The team interacts with the Sprint Backlog during the whole time of the sprint. The Sprint Backlog is great for following the progress of the sprint and organizing internally in the development team. 
  * **Sprint Retrospective**  - After a sprint ends there is a meeting where the Scrum Team gathers to discuss, to give feedback and improve based on the success of the sprint that has just finished. The goal of this meeting is to reflect on the sprint, on all the good things that happened, on all the shortcomings as well as to set some action points on which the whole team can act in order to improve and bring better value. This is usually done by:  
    * The typical Sprint Retrospective Model
        * What worked well in the sprint that ended
        * What can be improved ( usually things that didn't go well )
        * What can we commit to doing in the next sprint ( Action points for improvement )
    * 4L retrospective model
        * What we Liked about the sprint that ended
        * What we Lacked during the sprint that ended
        * What we Learned during the sprint that ended
        * What we Longed for 
* **Daily Scrum** - A short meeting of about 15 minutes that is used for the Scrum Team to sync on the work that has been done the previous day, as well as raising some discussion points for problems or unclear things. The meeting is done standing up, so that it is differentiated from other meetings on keeping it time-boxed in the 15 minutes and no more. These meetings are great for communicating issues or just setting a milestone in the sprint so that everyone is up to date on the progress.
### Flow
1. Defining Epics, Features and User Stories in the Product Backlog
2. Plan a new sprint and put stores from the Product Backlog in to the Sprint Backlog
3. Work on the sprint in the defined period of time
4. Have Daily Scrum meeting every day
5. Ship an incremental piece of the product
6. When the sprint is finished, have a Sprint Retrospective
7. Set action points to the Product Backlog
8. Repeat from step 2 and continue until the product is done

![scrum process flow](https://scrumorg-website-prod.s3.amazonaws.com/drupal/inline-images/ScrumFramework_2000x1000.png)
### Advantages
* **Iterative and incremental method:** allows tracking of a project workflow and provides intermediate results 
* **Adaptability for product development:** scrum allows you to change priorities and requirements. Also, add modifications or features quickly 
* **Participation and enhancing communication:** all team members are involved into process and motivated to express their opinion and contribute to all decisions. Team is able to easily communicate and eliminate obstacles as soon as possible 
* **Cooperation:** enhanced customer and client relationships by daily communication
* **Increasing productivity:** it allows to deliver products more quickly by determining an estimation and comparing the performance of team productivity
### Disadvantages 
* **Requires experienced team:** usually, scrum methodology is applied for small 5-8 people teams. Team members must be committed to the project, as this framework requires experienced team. If a team consists of novices in this area, there might be a risk of not to completing the project on time. Moreover, strict control over the team might put a lot of pressure on them which may also lead to failure 
* **Time expenses:** after each sprint a new sprint planning needs to be done as well as a retrospective meeting, which may consume a lot of time if a longer sprint is planned. Unexpected issues may also hinder the process of completing a sprint on time, thus more time will be needed to remove those issues 
* **Scope creep:** scrum doesn’t have a defined end date. For this reason, a released work may not have everything that the stakeholders want and new features are needed to be released 
* **Iteration definition:** the scrum estimation is one of the hardest and wasteful parts, as tasks must be well defined, otherwise estimated project costs and time will not be precise
# Similar processes 
## Kanban
Unlike Scrum, which defines specific time-boxed intervals of work and strict focus on the planned and decided tasks, Kanban focuses on organizing and managing the work as it moves through the development process. The goal of Kanban is to keep the process open so that when potential problems or bottlenecks occur, the work can be reorganized and changed accordingly. Kanban is often used with AGILE methodologies and there are some similarities with Scrum, like having a Backlog, a board with User Stories that are currently worked on. The way Kanban handles the work that is in progress is by limiting the Work In Progress board to a certain amount of tasks or complexity points of the tasks. This way there is always a manageable work on the board and expectations are managed accordingly. The board is divided by Backlog, Todo, Ongoing and Done tasks. The flow always goes from Backlog to Done and User stories are very rarely go back in the opposite direction. A kanban team also has a person that need to take care of the processes, sort of like a ScrumMaster. That's the process coach. It's responsibilities are very similar to the Scrum Master. 
### Advantages
* **Reduces waste and scrap:** products are manufactured only then when they are needed. In this way overproduction is eliminated 
* **Inventory level management:** Kanban has great inventory practices which smoothens out inventory levels and eliminates carrying costs 
* **Flexibility in production:** Kanban ensures you are not stuck with excess inventory if there is a sudden drop in demand for a product
* **Increases productivity and efficiency:** Kanban helps to eliminate the time waste and people are able to focus on current work. For this reason, making supplies is more accessible and productivity increases
### Disadvantages 
* **Less effective in shared-resource situations:** suppose that upward production line is made of several parts. Downsteam line requires make more parts, but it requires a buffer to ensure that downsteam station does not run out. System becomes more complex because each part needs a separate signaling card 
* **Inflexible in demand changes and product mix:** Kanban system assumes there is a stable production plan, where supplier always delivers components for production when it is needed. Therefore, the system is not suitable for industries where volumes and mixes of production fluctuate 
* **Variability elimination:** system can be disrupted by unpredictable and lengthy down times and any variability may affect system’s functions
* **Production flow:** because of the fact that Kanban requires planned weekly and monthly schedules linked with day-to-day flexibility, it may not be possible in an environment where multiple or short length product types are manufactured

## Scrumban
Scrumban is a mix of the Scrum and Kanban processes. Basically the structure of Scrum and the flexibility and visualization of Kanban put together in to one process. It is good for teams that work on a specific case that benefits from some of the features of the Scrum and Kanban processes, but not all. This is often used for teams that want to make a transition from one process to the other. This process has iteration planning and retrospectives to those iterations, but the iterations can be in different time-spans depending on the work and the organization at that particular time, like flexible sprints. There is analysis and prioritization before every iteration. But there are no clear roles for the people and the iteration is open to adding more stories if the work is not enough. There is only a limit on stories or complexity of the stories that are currently on the board. 
### Advantages
* **Saving time:** Scrumban uses planning on demand technique so there is no need to do estimating or sprint planning. Team plans only when there is a demand. For this reason team members get extra day of work 
* **Quality:** saved time on planning allows to focus on quality control and to verify if work item is ill-formed. Saved time allows to control a manufacturing process and to inspect if work is promoted to the ready queue. If something ill-formed is found, then it gets bounced and troubles are eliminated, then process is repeated once again 
* **Waste minimization:** Scrumban uses inter-process buffers and flow diagrams to show weaknesses and opportunities of the process. This gives an opportunity to eliminate everything that is not adding value to the customer

### Disadvantages 
* **Can turn into methodology mish-mash:** Scrumban is a new agile methodology that is a mixture of Scrum and Kanban and there are no clearly defined best practices. The basic principles of Scrumban are permitting and some teams may decide to invent on their own.
* **Hard to track individual team member effort and contribution:** Every team-member in Scrumban chooses their tasks themselves and there are no mandatory daily stand-up meetings which makes the tracking of what everyone did and plan to do next harder.

* **Less control for the project manager:** Since there is equality in the team, no daily-standups to report and everyone can assign tasks, the project manager’s control is limited. He can decide what to pick from the 3-month bucket, which tasks to schedule on the On-Demand Planning and how to prioritize them. From there the team members decide on their own how to handle and implement them.

* **Outdated Scrumban Board can cause issues:** Since every team-member chooses their tasks themselves, having an outdated board can cause trouble. For example, two members can start working on the same task or stay blocked because of a dependant task because the information on the board is not updated.

# Scrum Master role and responsibilities
The scrum master is responsible for making sure the Scrum process is followed and implemented as defined in the Scrum Guide. They focus on the team and making sure that everyone understands and implements the Scrum practices and rules as well as making sure that everyone in the team sees the value of the process. The scrum master must be present and vigilant in all parts of the team and bring value for:

* **The Product Owner** - Making sure that the scope of the work, the backlog items and logic is understood by everyone and that the requirements are set properly. Also it helps the product owner with organizing and managing the User Stories and the backlog
* **The Development Team** - The scrum master coaches and trains team members on the scrum processes as well as organization of current work. The Scrum Master also reacts and acts on bottle neck or blocking situations regarding the process so that the development team can work without roadblocks. They can also facilitate meetings on a need to basis.
* **The Organization** - Helps to coach the whole organization on the benefits of the scrum process and it's implementation.It also helps the organization with optimizing the productivity with managing the processes and making sure that everyone sees the 
## Organizing the sprint and events
Scrum events such as dailies, retrospectives, refinements etc. are managed by the scrum master and always have a clear goal. These events are also always time-boxed, meaning they take a finite amount of time that can be measured and planned before-hand. The sprint is also organized and kept in shape by the Scrum Master, documenting missing requirements, contacting the team or the product owner as well as finding blocking or stagnating issues. The board is also monitored by the scrum master in order to make sure that the goal of the sprint is not endangered, that the scope of work is not increased or decreased by a large margin and that the team members status is up to date with the current situation. 
## Sprint planning
The Scrum Master has a very important role in the sprint planning process. The Scrum Master must moderate this process and make sure that all the important questions get an answer and the plan is complete and ready. This can be done by making sure there is a discussion on few topics:

* What work items can be added in the following sprint - Discussion on picking user stories from the Product Backlog, assessing the difficulty and adding them to the sprint
* Is it possible and if so how the chosen items can be implemented - Discussing the complexity of all the stories, the capabilities of the team in the following period as well as planning the order and responsibilities regarding the stories added to the sprint
* What is the end-goal of this sprint - Discussing the end product that will be done when the sprint is complete, what value can it bring and what is the effect of this product or part of the product regarding the bigger picture
## Main responsibilities
* Making sure that they are in sync with the **product owner, the development team, the organization as well as the practices** that are being used
* Ensures the entire team supports the chosen  **Scrum process;**
* Manages the  **impediments**  that exceed the self-organizing capabilities of the team and it prevents them in achieving the Sprint Goal;
* Recognizes  **healthy team conflict**  and promotes constructive disagreement;
* Is prepared to be  **disruptive**  enough to enforce a change within the organization;
* Understands the power of a  **self-organization;**
* Understands the value of a  **steady sprint rhythm**  and does everything to create and maintain it;
* Knows how to  **truly listen**  and is comfortable with silence;
* **Teaches the Product Owner**  how to maximize ROI and meet objectives;
-   Can easily transfer their knowledge to other processes such as **XP, Kanban and Lean.**

# Scrum Master role and responsibilities in other companies
* The Scrum Master role is always tailored to the team and the product, not the rules and limitations 
* Some companies have different roles for Scrum Master for different teams of the company
* The role is built along the way, by observing, analyzing and doing what is right for the team
* Things that stay consistent between multiple company Scrum Master roles are:
    * Mediation between members of the team and the product owner 
    * Making sure that everyone understands, follows and knows the benefits of the set processes
    * Taking care of the expectations and requirements
* Things that diverge from the general Scrum Master role are:
    * Length of meetings
    * Scheduling retrospectives
          * Two types of retrospectives: mini ( every member addresses 1 thing ) and regular ( longer and more discussion )
          * Less frequent retrospectives
    
# Scrum Master responsibilities in Brainnet team
* Deploy, manage and document releases
* Document retrospectives, dailies, demo showcases and processes
* Managing the board and tasks
* Planning and modifying sprints
* Organizing meetings and mediating communication
* Implement and document processes
* Merge code and manage branching model
* Documenting and managing extra requirements
* Managing team and product owner expectations and requests

## What we currently have implemented
* **Daily meetings**
    * Kept short under 30 minutes
    * Not blocked with issues, issues are delegated to Azure Devops
    * Time for discussion on important topics
    * Work and notes documented
* **Retrospectives**
	* New ideas and suggestions
	* Working on things that were a problem in previous sprints
	* Less problems and issues during sprints
	* Well documented
* **Demo**
	* Change requests are brought to minimum
	* Before a demo we deliver a list of changes that we will show so we set expectations
	* Discussions are on point
* **Sprint Planning**
	* Sprints are planned with estimations on time and complexity
	* Stories are covered again by the team
	* Board is updated
* **Feedback and Communication**
	* Feedback is frequently given on refinement meetings, demos and comments
	* Questions regarding the business are directly asked on the user story instead of the daily
	* Change requests are documented on the stories or as new User Stories
	* Clarifications are discussed on the user story comments
* **Refinement**
	* Refinement is done on the teams part
	* During team refinement, stories are given complexity level
	* A refinement meeting is done with the business
* **Working with the board**
	* Statuses are used frequently and everyone is in sync in using them
	* Board is updated frequently 
	* Stories are added and kept updated, with extra activities such as working on new processes or technologies
* **More releases**
	* Releases are done more frequently
	* Release notes are well documented
	* Naming and release procedure is well defined
* **Testng**
	* Testing is in sync most of the time
	* Test scenarios are written
	* Started using repro steps for scenarios
## What we can improve
* **Better sync with other team**
	* Sync on large deployments and changes
	* Sync on changes to technology or configurations
* **Better deployment process**
	* The deployment process is still slow
	* Merging is still a hassle
* **Complete testing**
	* Automated tests
	* User stories and bugs to have fully documented scenarios and repro steps
* **Sprints to be closer to their goal**
	* Stories are planned for 2-3 sprints ahead
	* A lot of stories are changing form one to other sprints
* **Implementing internal reviews**
	* There is still no internal code reviews implemented
* **Better documentation on common issues**
	* Common issues that happen while developing are not documented
	* Some known processes or settings for boabaas are not documented 
